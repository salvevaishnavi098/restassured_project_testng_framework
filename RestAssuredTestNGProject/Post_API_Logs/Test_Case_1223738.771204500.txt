Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Fri, 08 Mar 2024 17:07:37 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"880","createdAt":"2024-03-08T17:07:37.352Z"}