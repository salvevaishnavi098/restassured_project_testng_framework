Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Fri, 08 Mar 2024 17:06:58 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"304","createdAt":"2024-03-08T17:06:58.488Z"}