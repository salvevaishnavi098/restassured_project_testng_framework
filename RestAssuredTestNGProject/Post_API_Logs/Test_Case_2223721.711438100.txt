Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Yash",
    "job": "qa"
}

Response header date is : 
Fri, 08 Mar 2024 17:07:20 GMT

Response body is : 
{"name":"Yash","job":"qa","id":"903","createdAt":"2024-03-08T17:07:20.355Z"}